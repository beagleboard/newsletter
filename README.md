# BeagleBoard.org Foundation Newsletter
![Build Status](https://openbeagle.org/newsletter/newsletter.beagleboard.io/badges/main/pipeline.svg)

Moved to https://openbeagle.org/newsletter/newsletter.beagleboard.io

See live site at https://newsletter.beagleboard.io
